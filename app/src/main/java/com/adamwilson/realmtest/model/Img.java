package com.adamwilson.realmtest.model;

import io.realm.RealmObject;

public class Img extends RealmObject {

    public String title;
    public String url;

    @Override public String toString() {
        return "Img{" +
               "title='" + title + '\'' +
               ", url='" + url + '\'' +
               '}';
    }

}
