package com.adamwilson.realmtest;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;

public class RealmTestApp extends Application {

    @Override public void onCreate() {
        super.onCreate();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                      .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                      .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                      .build());

        Realm.init(this);
    }
}
