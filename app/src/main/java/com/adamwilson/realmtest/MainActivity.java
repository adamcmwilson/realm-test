package com.adamwilson.realmtest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.adamwilson.realmtest.model.Img;
import com.bumptech.glide.Glide;

import hugo.weaving.DebugLog;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private Realm realm;

    private GridView gridView;

    @DebugLog @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView = (GridView) findViewById(R.id.grid);

        realm = Realm.getDefaultInstance();

        clearAllData();
        initData();
        loadGrid();
    }

    @DebugLog @Override protected void onDestroy() {
        super.onDestroy();
        realm.close(); // Remember to close Realm when done.
    }

    @DebugLog private void clearAllData() {
        begin();
        realm.deleteAll();
        commit();
    }

    @DebugLog private void initData() {

        final String[] titles = {
                "Little Red Spot",
                "Dawn XMO2 Image 30",
                "Tethys",
                "Pluto",
                "Jupiter",
                "Bedrock Exhumed from the Deep",
                "Gyrating Active Region",
                "Pits and Channels of Hebrus Valles",
                "Supernova SN 2014C (X-ray)",
                "Opportunity for Moon-Gazing",
                "Crescent Jupiter"
        };

        final String[] urls = {
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA21378_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA21250_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA20518_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA11709_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA21377_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA12291_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA11703_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA11704_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA21089_hires.jpg",
                "http://www.jpl.nasa.gov/spaceimages/images/largesize/PIA12290_hires.jpg",
                "http://photojournal.jpl.nasa.gov/jpeg/PIA21376.jpg"
        };

        begin();
        for (int i = 0, max = titles.length; i < max; i++) {
            addImg(titles[i], urls[i]);
        }
        commit();
    }

    @DebugLog private void begin() { realm.beginTransaction(); }

    @DebugLog private void commit() { realm.commitTransaction(); }

    @DebugLog private void addImg(String title, String url) {
        Img img = realm.createObject(Img.class);
        img.title = title;
        img.url = url;
    }

    @DebugLog private void loadGrid() {
        RealmResults<Img> imgs = realm.where(Img.class).findAll();
        gridView.setAdapter(new GridAdapter(getApplicationContext(), imgs));
    }

    private class GridAdapter extends RealmBaseAdapter<Img> {

        @DebugLog public GridAdapter(@NonNull Context context,
                                     @Nullable OrderedRealmCollection<Img> data) {
            super(context, data);
        }

        @DebugLog @Override public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext())
                                            .inflate(R.layout.item_img, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.txt = (TextView) convertView.findViewById(R.id.txt);
                viewHolder.img = (ImageView) convertView.findViewById(R.id.img);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Img item = adapterData.get(position);
            viewHolder.txt.setText(item.title);

            Glide.with(parent.getContext())
                 .load(item.url)
                 .into(viewHolder.img);

            return convertView;
        }

        private class ViewHolder {
            TextView  txt;
            ImageView img;
        }
    }


}

